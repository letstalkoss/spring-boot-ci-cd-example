FROM openjdk:17-jdk

WORKDIR /app

COPY /target/spring-data-jpa-mysql-1.0.jar spring-data-1.0.0.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","./spring-data-1.0.0.jar"]
